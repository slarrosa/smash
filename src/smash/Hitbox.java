package smash;

import java.awt.Color;

import Core.Sprite;

public class Hitbox extends Sprite{

	int frames = 10;
	/**
	* A la funcio Hitbox creem el constructor de la hitbox, o sigui els atacs basics dels personatges
	*/
	public Hitbox(String name, int x1, int y1, int x2, int y2, double angle) {
		super(name, x1, y1, x2, y2, angle, new Color(255, 0, 0, 150));
		// TODO Auto-generated constructor stub
		this.physicBody=true;
		this.terrain=false;
		this.trigger=true;
	}
	/**
	* A la funcio frame fem un contador per calcular el temps que ha de estar la
	* hitbox de l'atac.
	*/
	public void frame() {
		// TODO Auto-generated method stub
		frames-=1;
		if(frames==0) {
			this.delete();
		}
		
	}
	/**
	* A la funcio comprobarColision fa que quan peguem al pikachu rebi mal i el moguem
	*/
	public void comprobarColision(Personaje otro) {
		if(this.collidesWith(otro)){
			boolean golpeado = false;
			otro.dano+=10;
			System.out.println("pum "+otro.dano);
			otro.addForce(0.1*otro.dano, -0.1*otro.dano);

		}
	}
	/**
	* A la funcio comprobarColision fa que quan peguem al Kirby rebi mal i el moguem
	*/
	public void comprobarColision2(Personaje otro) {
		if(this.collidesWith(otro)){
			
			otro.dano+=10;
			System.out.println("pum "+otro.dano);
			otro.addForce(0, -0.1*otro.dano);
			
		}
	}

}
