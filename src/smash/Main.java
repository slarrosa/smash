package smash;

import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;
import smash.Personaje;
import smash.Hitbox;
import smash.Suelo;

public class Main {
	public static Field f = new Field();
	public static Window w = new Window(f);
	public static int mainX = 50;
	public static int mainY = 50;
	public static Personaje kirby = new Personaje("Kerba", mainX, mainY, mainX+80, mainY+120, 0, "kirby.gif");
	public static Suelo suelo = new Suelo("Suelo", 55, 250, 580, 300, " ");
	public static Suelo platafrma1 = new Suelo("plataforma1", 65, 135, 80, 300, " ");
	public static Personaje pikachu = new Personaje("Pokachu", 300, 50, 380, 170, 0, "pikachu.gif");
	public static ArrayList<Hitbox> listHitbox = new ArrayList<Hitbox>();
	public static boolean fi = true;
	/**
	* Al main omplim la array de sprites, afegim les forces i tenim el bucle principal del joc.
	*/
	public static void main(String[] args) throws InterruptedException {
		f.background = "background.jpg";
		kirby.addConstantForce(0, 1.2);
		pikachu.addConstantForce(0, 1.2);
		ArrayList<Sprite> sprites = new ArrayList<Sprite>();
		sprites.add(kirby);
		sprites.add(suelo);
		sprites.add(pikachu);

		while (fi) {
			System.out.println(kirby.x1);
			sprites.addAll(listHitbox);
			//System.out.println(listHitbox.size());
			input();
			System.out.println(fi);
			input2();
			Personaje.desaparecerBordes();
			
			for (Hitbox h : listHitbox) {
				h.frame();
			}
			if (kirby.isGrounded(f)) {
				kirby.aterra = true;
			}
			if(pikachu.isGrounded(f)) {
				pikachu.aterra=true;
			}
			f.draw(sprites);
			
			Thread.sleep(30);
			if(pikachu.isGrounded(Main.f)) {
				pikachu.setForce(0, 0);
			}
		}

	}
	/**
	 * Aquesta funcio es la que fa tots els imputs del Pikachu, moure's, pegar i saltar.
	 */
	private static void input2() {
		if (w.getPressedKeys().contains('8')) {
			pikachu.jump();
		}
		if (w.getPressedKeys().contains('6')) {
			pikachu.moveRight();
		}
		if (w.getPressedKeys().contains('4')) {
			pikachu.moveLeft();
		}
		if (!w.getPressedKeys().contains('6') && !w.getPressedKeys().contains('4')) {
			pikachu.doNotMove();
		}
		if (w.getKeysDown().contains('0')) {
			Hitbox h2 = pikachu.pegar();
			h2.comprobarColision2(kirby);
			listHitbox.add(h2);
		}
	}
	/**
	 * Aquesta funcio es la que fa tots els imputs del Kirby, moure's, pegar i saltar.
	 */
	private static void input() {

		if (w.getPressedKeys().contains('w')) {
			kirby.jump();
		}
		if (w.getPressedKeys().contains('d')) {
			kirby.moveRight();
		}
		if (w.getPressedKeys().contains('a')) {
			kirby.moveLeft();
		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			kirby.doNotMove();
		}
		if (w.getKeysDown().contains(' ')) {
			Hitbox h = kirby.pegar();
			h.comprobarColision(pikachu);
			listHitbox.add(h);

		}

	}

}
