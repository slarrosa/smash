package smash;

import Core.Sprite;

public class Personaje extends Sprite {

	int dano = 0;
	static int vidasKirby = 3;
	static int vidasPikachu = 3;
	boolean aterra = false;

	/**
	 * A la funcio Kirby creem el constructor dels personatges.
	 */
	public Personaje(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		// TODO Auto-generated constructor stub
		this.physicBody = true;
		this.terrain = false;
		this.trigger = false;
	}

	/**
	 * A la funcio jump creem els salts
	 */
	public void jump() {
		if (aterra == true) {
			setForce(0, -5.5);
			aterra = false;
		}

	}

	/**
	 * A la funcio moveRight creem el moviment cap a la dreta
	 */
	public void moveRight() {
		setVelocity(2, velocity[1]);
	}

	/**
	 * A la funcio moveLeft creem el moviment cap a la esquerra
	 */
	public void moveLeft() {
		setVelocity(-2, velocity[1]);
	}

	/**
	 * A la funcio doNotMove fem que si el personatje es al terre no es mogui.
	 */
	public void doNotMove() {
		// TODO Auto-generated method stub
		if (aterra) {
			setVelocity(0, velocity[1]);
		}

	}

	/**
	 * La funcio pegar crea un cuadrat amb un hitbox.
	 */
	public Hitbox pegar() {
		Hitbox h = new Hitbox("hitbox", x2, (y1 + y2) / 2, x2 + 50, ((y1 + y2) / 2) + 30, 0);
		return h;
	}

	/**
	 * La funcio desaparecerBordes fa que els personatjes desapareguin al passarse
	 * del mapa i reapareguin a la posicio principal, a m�s de baixarli les vides i
	 * si arriben a 0 s'acaba la partida.
	 */
	public static void desaparecerBordes() {
		if (Main.kirby.y1 > 350 || Main.kirby.x1> 630 || Main.kirby.x1< -70) {
			Main.kirby.x1 = Main.mainX;
			Main.kirby.x2 = Main.mainX + 80;
			Main.kirby.y1 = Main.mainY;
			Main.kirby.y2 = Main.mainY + 120;
			vidasKirby -= 1;
			Main.kirby.dano=0;
			System.out.println(vidasKirby);
		}
		if (vidasKirby <= 0) {
			Main.fi = false;
			Main.w.close();
		}
		if (Main.pikachu.y1 > 350 || Main.pikachu.x1> 630 || Main.pikachu.x1< -70) {
			Main.pikachu.x1 = 300;
			Main.pikachu.x2 = 380;
			Main.pikachu.y1 = 50;
			Main.pikachu.y2 = 170;
			vidasPikachu -= 1;
			Main.pikachu.dano=0;
			System.out.println(vidasPikachu);
		}
		if (vidasPikachu <= 0) {
			Main.fi = false;
			Main.w.close();
		}

	}
}